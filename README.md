## libpng proxy repository for hell

This is proxy repository for [libpng library](http://www.libpng.org/pub/png/), which allow you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of libpng using hell, have improvement idea, or want to request support for other versions of libpng, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in libpng itself please create issue on [libpng issue tracker](https://github.com/glennrp/libpng/issues), because here we don't do any kind of libpng development.
